﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daxi
{
    class NeuronalNetwork
    {
        private List<InputNeuron> inputNeurons = new List<InputNeuron>();
        private List<WorkingNeuron> hiddenNeurons = new List<WorkingNeuron>();
        private List<WorkingNeuron> outputNeurons = new List<WorkingNeuron>();

        private void AddInputNeuron(InputNeuron neuron)
        {
            inputNeurons.Add(neuron);
        }

        private void AddHiddenNeuron(WorkingNeuron neuron)
        {
            hiddenNeurons.Add(neuron);
        }

        private void AddOutputNeuron(WorkingNeuron neuron)
        {
            outputNeurons.Add(neuron);
        }

        public void GenerateFullMesh()
        {
            foreach (WorkingNeuron wn in hiddenNeurons)
            {
                foreach (InputNeuron input in inputNeurons)
                {
                    wn.AddNeuronConnection(input, 1);
                }
            }

            foreach (WorkingNeuron wn in outputNeurons)
            {
                foreach (WorkingNeuron wm2 in hiddenNeurons)
                {
                    wn.AddNeuronConnection(wm2, 1);
                }
            }
        }
    }
}
//Listen für die Neuronen
//Add Funktionen
//Generieren Funktionen (Mesh, Hidden Neurons etc)