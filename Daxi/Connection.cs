﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daxi
{
    public class Connection
    {
        private float weight = 1;
        public Neuron entrieNeuron;

        public Connection(Neuron n, float weight = 1)
        {
            this.weight = weight;
            this.entrieneutron = n;
        }
        
        public float GetValue()
        {
        return weight * entrieNeuron.GetValue(); 
        }
    }
}