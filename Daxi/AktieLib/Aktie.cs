﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSVLib;


namespace AktieLib
{
    public class Aktie
    {

        public List<int> date_day = new List<int>();
        public List<int> date_month = new List<int>();
        public List<int> date_year = new List<int>();
        public List<double> value_adjclose = new List<double>();
        public List<double> value_close = new List<double>();
        public List<double> value_high = new List<double>();
        public List<double> value_open = new List<double>();
        public List<double> value_low = new List<double>();
        public List<double> value_volume = new List<double>();
        public string name;
        public int countvalues = 0;

        Aktie[] Verzeichnis = new Aktie[10];

        private int count=0;

        public Aktie TeachAktie(CSVReader Input)
        {
            Aktie a = new Aktie();
  
            a.date_year = Input.ConvertToInt(Input.GetCSVValues(1));
            a.date_month = Input.ConvertToInt(Input.GetCSVValues(2));
            a.date_day = Input.ConvertToInt(Input.GetCSVValues(3));
            a.value_open = Input.ConvertToDouble(Input.GetCSVValues(4));
            a.value_high = Input.ConvertToDouble(Input.GetCSVValues(5));
            a.value_low = Input.ConvertToDouble(Input.GetCSVValues(6));
            a.value_close = Input.ConvertToDouble(Input.GetCSVValues(7));
            a.value_adjclose = Input.ConvertToDouble(Input.GetCSVValues(8));
            a.value_volume = Input.ConvertToDouble(Input.GetCSVValues(9));
            a.name = Input.name;
            a.countvalues = Input.count_values;
            Verzeichnis[count] = a;
            

            count++;
            return a;
        }

        public int GetCount()
        {
              return count;
        }

        public Aktie[] GetAktien()
        {
            return Verzeichnis;
        }

        public double GetMin(Aktie a, date start, date end)
        {
            double min = 0;
            if (start.year == 0)
            {
                min = a.value_close[0];
                for (int i = 0; i < a.countvalues; i++)
                {
                    if (min > a.value_close[i])
                        min = a.value_close[i];
                }
            }
            else
            {
                int initstart = 0;
                for (int i = 0; i < a.countvalues; i++)
                {
                    if ((a.date_day[i] == start.day) && (a.date_month[i] == start.month) && (a.date_year[i] == start.year))
                    {
                        initstart = 1;
                    }
                    if (initstart == 1)
                    {
                        min = a.value_close[i];
                        if (min > a.value_close[i])
                            min = a.value_close[i];
                    }

                    if ((a.date_day[i] == end.day) && (a.date_month[i] == end.month) && (a.date_year[i] == end.year))
                    {
                        initstart = 0;
                    }

                }

                                }
            return min;
        }

        public double GetMax(Aktie a, date start, date end)
        {
            double max = 0;
            if (start.year == 0)
            {
                max = a.value_close[0];
                for (int i = 0; i < a.countvalues; i++)
                {
                    if (max < a.value_close[i])
                        max = a.value_close[i];
                }
            }
            else
            {
                int initstart = 0;
                for (int i = 0; i < a.countvalues; i++)
                {
                    if ((a.date_day[i] == start.day) && (a.date_month[i] == start.month) && (a.date_year[i] == start.year))
                    {
                        initstart = 1;
                    }
                    if (initstart == 1)
                    {
                        max = a.value_close[i];
                        if (max < a.value_close[i])
                            max = a.value_close[i];
                    }

                    if ((a.date_day[i] == end.day) && (a.date_month[i] == end.month) && (a.date_year[i] == end.year))
                    {
                        initstart = 0;
                    }
                }
            }
            return max;

         }



        public Aktie CompareAktie(Aktie a, Aktie b, Aktie c, int method, date start, date end)
        {

            switch (method)
            {
                case 1:
                    { // method 1 - absolute Differenz der Aktien
                        double a1min = a.GetMin(a,start,end);
                        double a1max = a.GetMax(a,start,end);
                        double b1min = b.GetMin(b,start,end);
                        double b1max = b.GetMax(b,start,end);
                        double[] adiff = null;
                        double[] bdiff = null;                       
                                            
                        for (int k = 0; a.countvalues >= k; k++)
                        {
                            adiff[k] = a.value_close[k] - a1min;
                            bdiff[k] = b.value_close[k] - b1min;
                            c.value_close[k] = adiff[k] - bdiff[k];
                        }
                        return c;    
                    }
                case 2:
                    { // method 2 - prozentuale Differenz der Aktienschwankung
                        double a1min = a.GetMin(a, start, end);
                        double a1max = a.GetMax(a, start, end);
                        double b1min = b.GetMin(b, start, end);
                        double b1max = b.GetMax(b, start, end);
                        double[] adiff = null;
                        double[] bdiff = null;

                        for (int k = 0; a.countvalues >= k; k++)
                        {
                            adiff[k] = (a.value_close[k] - a.value_close[k + 1]) / a.value_close[k];
                            bdiff[k] = (b.value_close[k] - b.value_close[k + 1]) / b.value_close[k];
                            c.value_close[k] = adiff[k] - bdiff[k]; //prozentuale Veränderung wohin?
                        }
                        return c;
                    }
            }
            
           

            return a;
        }

        // Aktienverzeichnis nur außerhalb erstellen 
        // Anzahl mit mitgegebenem Verzeichnis bestimmen

    }
    public class date
    {
        public int day;
        public int month;
        public int year;
    }
}
