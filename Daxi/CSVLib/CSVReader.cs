﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CSVLib
{
    public class CSVReader
    {
        public List<string> list_date = new List<string>();
        public List<string> list_day = new List<string>();
        public List<string> list_month = new List<string>();
        public List<string> list_year = new List<string>();

        public List<string> list_open = new List<string>();
        public List<string> list_high = new List<string>();
        public List<string> list_low = new List<string>();
        public List<string> list_close = new List<string>();
        public List<string> list_adjclose = new List<string>();
        public List<string> list_volume = new List<string>();
        public string name;

        public int count_values;
        StreamReader streamreader;

        public void ReadCSV(string file, string _name)
        {
            // CSVReader back = new CSVReader();

            if (System.IO.File.Exists(file))
            {
                //  byte[] byteArray = Encoding.UTF8.GetBytes(file);
                //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
                // MemoryStream stream = new MemoryStream(byteArray);

                streamreader = new StreamReader(file);
                //  StreamReader streamreader = new StreamReader(file);
                count_values = 0;
                while (!streamreader.EndOfStream)
                {
                    var line = streamreader.ReadLine();
                    var values = line.Split(',');
                    if (count_values == 0) { }
                    else
                    {
                        list_date.Add(values[0]);
                        list_open.Add(values[1]);
                        list_high.Add(values[2]);
                        list_low.Add(values[3]);
                        list_close.Add(values[4]);
                        list_adjclose.Add(values[5]);
                        list_volume.Add(values[6]);
                        var linedates = values[0].Split('-');
                        list_year.Add(linedates[0]);
                        list_month.Add(linedates[1]);
                        list_day.Add(linedates[2]);
                    }
                    count_values++;
                }
                count_values--;
                name = _name;
            }
        }

        public List<string> GetCSVValues(int index)
        {
            // List<string> error;
            // error.Add("0");

            switch (index)
            {

                case 1:
                    return list_year;
                case 2:
                    return list_month;
                case 3:
                    return list_day;
                case 4:
                    return list_open;
                case 5:
                    return list_high;
                case 6:
                    return list_low;
                case 7:
                    return list_close;
                case 8:
                    return list_adjclose;
                case 9:
                    return list_volume;
                default:   // hier error code
                    return list_date;
            }
        }
        public List<int> ConvertToInt(List<string> input)
        {
            List<int> back = new List<int>();

            for (int i = 0; i < count_values - 1; i++)
            {
                back.Add(Int32.Parse(input[i]));
            }
            return back;
        }

        public List<double> ConvertToDouble(List<string> input)
        {
            List<double> back = new List<double>();

            for (int i = 0; i < count_values - 1; i++)
            {
                back.Add(double.Parse(input[i].Replace(".", ",")));
            }
            return back;
        }



    }
}
