﻿/* using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Anzeige
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}

*/




using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace Wpf.CartesianChart.Financial
{
    public partial class OhclExample : UserControl, INotifyPropertyChanged
    {
        private string[] _labels;

        public OhclExample()
        {
            InitializeComponent();

            SeriesCollection = new SeriesCollection
            {
                new OhlcSeries()
                {
                    Values = new ChartValues<OhlcPoint>
                    {
                        new OhlcPoint(32, 35, 30, 32),
                        new OhlcPoint(33, 38, 31, 37),
                        new OhlcPoint(35, 42, 30, 40),
                        new OhlcPoint(37, 40, 35, 38),

                        new OhlcPoint(33, 38, 30, 37),
                        new OhlcPoint(33, 40, 32, 37),
                        new OhlcPoint(33, 35, 29, 37),
                        new OhlcPoint(33, 39, 28, 37),
                        new OhlcPoint(33, 42, 34, 37),
                        new OhlcPoint(33, 41, 31, 37),
                        new OhlcPoint(33, 39, 35, 37),
                        new OhlcPoint(33, 38, 34, 37),
                        new OhlcPoint(33, 36, 31, 37),
                        new OhlcPoint(33, 39, 30, 37),
                        new OhlcPoint(33, 38, 31, 37),
                        new OhlcPoint(33, 35, 32, 37),
                        new OhlcPoint(33, 35, 31, 37),
                        new OhlcPoint(33, 38, 36, 37),
                        new OhlcPoint(33, 37, 31, 37),

                        new OhlcPoint(35, 38, 32, 33)
                    }
                },

            };

            Labels = new[]
            {
                DateTime.Now.ToString("dd MMM"),
                DateTime.Now.AddDays(1).ToString("dd MMM"),
                DateTime.Now.AddDays(2).ToString("dd MMM"),
                DateTime.Now.AddDays(3).ToString("dd MMM"),
                DateTime.Now.AddDays(4).ToString("dd MMM"),
            };

            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }

        public string[] Labels
        {
            get { return _labels; }
            set
            {
                _labels = value;
                OnPropertyChanged("Labels");
            }
        }

        private void UpdateAllOnClick(object sender, RoutedEventArgs e)
        {
            var r = new Random();

            foreach (var point in SeriesCollection[0].Values.Cast<OhlcPoint>())
            {
                point.Open = r.Next((int)point.Low, (int)point.High);
                point.Close = r.Next((int)point.Low, (int)point.High);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}