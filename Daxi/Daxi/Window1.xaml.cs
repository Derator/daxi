﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AktieLib;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;




namespace Daxi

{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        Aktie[] Verzeichnis = new Aktie[10];
        int[] compareIndex = new int[2];
        int comparecount = 0;
            
        public Window1(Aktie[] Verzeichnis, int anz)
        {
            InitializeComponent();
            
            //Verzeichnis[0].GetAktien();
            for (int i = 0; i<anz;i++)

            Aktienwahl.Items.Add(Verzeichnis[i].name);
  
            // TestWin1.Text = Convert.ToString(Verzeichnis[0].GetCount());
        }

        private void Aktienwahl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (comparecount < 2)
            {
                Vergleichliste.Items.Add(Aktienwahl.SelectedIndex);
                compareIndex[comparecount] = Aktienwahl.SelectedIndex;
                Vergleichliste.Items.Add(compareIndex[comparecount]);
                comparecount++;
            }
            
        }

        private void anzeigen_Click(object sender, RoutedEventArgs e)
        {
            UserControl Anzeige = new UserControl1();
            Anzeige.UpdateLayout();
            //Window Anzeige = new Window4();
          //  Anzeige.Show();
        }
    }
}
