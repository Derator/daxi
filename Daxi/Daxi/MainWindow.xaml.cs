﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using CSVLib;
using AktieLib;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;


namespace Daxi
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        CSVReader Read = new CSVReader();
        Aktie InAktie = new Aktie();
        Aktie[] Verzeichnis = new Aktie[10];

        public MainWindow()
        {
            InitializeComponent();

        }

        private void ReadCSVtoAktie_Click(object sender, RoutedEventArgs e)
        {
            //SeriesCollection coll = new SeriesCollection();

            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();

            Read.ReadCSV(open.FileName, open.SafeFileName);
            Verzeichnis[InAktie.GetCount()] = InAktie.TeachAktie(Read);


            //InAktie = InAktie.TeachAktie(Read);

            //Debug.Text = Convert.ToString(Verzeichnis[0].date_day[0]);
            Debug.Text = Convert.ToString(InAktie.GetCount());



        }

        private void ShowAktien_Click(object sender, RoutedEventArgs e)
        {

            //anzeigefenster anzeige = new anzeigefenster();
            //  anzeige.Show();

            Window1 neu = new Window1(Verzeichnis, InAktie.GetCount());
            neu.Show();

        }

        private void Zeitraumauswahl_Click(object sender, RoutedEventArgs e)
        {
            Window Zeitraum = new Window2();
            Zeitraum.Show();
        }
    }
}
